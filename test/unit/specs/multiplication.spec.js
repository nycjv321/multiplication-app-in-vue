import { calculate, Parser } from '@/multiplication'
const { Decimal } = require('decimal.js')

describe('multiplication', () => {
  describe('Parser', () => {
    var parser

    beforeEach(function () {
      parser = new Parser()
    })
    describe('extractNumbers', () => {
      it('should nullify invalid input', () => {
        expect(parser.extractNumbers(1))
          .toEqual(null)
      })
      it('should extractNumbers a number', () => {
        expect(parser.extractNumbers('1'))
          .toEqual([new Decimal(1)])
      })
      it('should extractNumbers numbers with commas', () => {
        expect(parser.extractNumbers('1,2,3'))
          .toEqual([new Decimal(1), new Decimal(2), new Decimal(3)])
      })
      it('should extractNumbers numbers with commas with spaces', () => {
        expect(parser.extractNumbers('1,2, 3'))
          .toEqual([new Decimal(1), new Decimal(2), new Decimal(3)])
      })
      it('should extractNumbers numbers with newlines', () => {
        expect(parser.extractNumbers('1\u000A2\u000A\u000A42'))
          .toEqual([new Decimal(1), new Decimal(2), new Decimal(42)])
      })
      it('should extractNumbers numbers with newlines', () => {
        expect(parser.extractNumbers('1,2\u000A\u000A42'))
          .toEqual([new Decimal(1), new Decimal(2), new Decimal(42)])
      })
    })
    describe('extractInput', () => {
      it('should parse numbers', () => {
        let parser = new Parser()
        expect(parser.extractInput(1))
          .toEqual([1])
      })
      it('should parse strings', () => {
        let parser = new Parser()
        expect(parser.extractInput('a'))
          .toEqual(['a'])
      })
      it('should parse stings with commas', () => {
        let parser = new Parser()
        expect(parser.extractInput('a, b, c'))
          .toEqual(['a', 'b', 'c'])
      })
      it('should parse stings with newlines', () => {
        let parser = new Parser()
        expect(parser.extractInput('a\u000Ac'))
          .toEqual(['a', 'c'])
      })
      it('should parse stings with commas and new lines', () => {
        let parser = new Parser()
        expect(parser.extractInput('a, b\u000Ac'))
          .toEqual(['a', 'b', 'c'])
      })
    })
  }),
  describe('calculate', () => {
    it('calculate no value', () => {
      expect(calculate(''))
        .toEqual(null)
    })
    it('calculate 1 value', () => {
      expect(calculate([new Decimal(1)]))
        .toEqual(new Decimal(1))
    })
    it('calculate many values', () => {
      expect(calculate([new Decimal(1), new Decimal(2), new Decimal(3)]))
        .toEqual(new Decimal(6))
    })
    it('handle invalid input', () => {
      expect(calculate([new Decimal(1), 'z', new Decimal(3)]))
        .toEqual('z')
    })
  })
})
