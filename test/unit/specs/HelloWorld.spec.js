import Vue from 'vue'
import MultiplicationApp from '@/components/multiplication-app'

describe('HelloWorld.vue', () => {
  it('should render correct contents', () => {
    const vm = new Vue(MultiplicationApp).$mount()
    vm.$nextTick(() => {
      expect(vm.$el).toMatchSnapshot();
    });
  })
})
