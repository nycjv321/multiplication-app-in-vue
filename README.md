# multiplication-app

> a simple multiplication-only calculator

## Requirements
### Node

node 12.x or greater is required. If you don't have a recent version of node installed please see the instructions on using this app through Docker

## Build Setup

### install dependencies

You can use `npm install`. Alternatively, 
you can use `yarn install` if you use yarn 
instead of npm for package management. 
See https://yarnpkg.com/en/ for more details
 
     npm install   

### Serve with hot reload at localhost:8080
  
    npm run dev

### build for production with minification
  
    npm run build

### build for production and view the bundle analyzer report
  
    npm run build --report

### run unit tests
  
    npm run unit

### run e2e tests
  
    npm run e2e

### run all tests
  
    npm test

#### Test output    
    
    -------------------------|----------|----------|----------|----------|-------------------|
    File                     |  % Stmts | % Branch |  % Funcs |  % Lines | Uncovered Line #s |
    -------------------------|----------|----------|----------|----------|-------------------|
    All files                |    59.62 |       68 |    69.23 |    55.32 |                   |
     src                     |    93.94 |    89.47 |      100 |    92.86 |                   |
      multiplication.js      |    93.94 |    89.47 |      100 |    92.86 |             41,46 |
     src/components          |        0 |        0 |        0 |        0 |                   |
      error-notification.vue |        0 |      100 |        0 |        0 |                12 |
      multiplication-app.vue |        0 |        0 |        0 |        0 |... 49,50,51,52,53 |
      notification.vue       |        0 |      100 |        0 |        0 |                11 |
    -------------------------|----------|----------|----------|----------|-------------------|
    
    Test Suites: 2 passed, 2 total
    Tests:       16 passed, 16 total
    Snapshots:   1 passed, 1 total
    Time:        3.451s

## Software Requirements

This app supports fairly large numbers but will eventually treat everything in scientific notation.

It should be pretty good about reporting errors. It supports both comma delimited and newline delimited content. For example

    1,2
    3
    
becomes `1 x 2  x 3 = 6`. 

It does not _automagically_ handle instances where a comma is omitted.

    1 2

The above input is considered invalid.  But the input below is considered valid:

    1, 2
    
 
## Tests

### Newlines

`cucumber-js` is a little weird about newline in parameters. You should notice that the tests 
will convert the string `{{NEWLINE}}` to a literal `\n` for you.

For example:

    1{{NEWLINE}},,,5,-100
   
Becomes:

    1
    ,,,5,-100
    
You should see similar behavior for empty strings and emoji(s)

## Docker

### Building

    docker build -t multiplication-app .

### Running

    docker run -d --name multiplication-app -p 8080:80 multiplication-app 

#### Quick Check

The above command should've created a docker container named "multiplication-app" on port :8080. On my machine, I see:
 
     docker ps
      CONTAINER ID        IMAGE                COMMAND                  CREATED             STATUS              PORTS                    NAMES
      931fbd8b6082        multiplication-app   "docker-entrypoint.s…"   4 seconds ago       Up 3 seconds        0.0.0.0:8080->8080/tcp   multiplication-app

We can check this real quick with  `curl`:

    curl 127.0.0.1:8080 -i -vvv > /dev/null
    * Rebuilt URL to: 127.0.0.1:8080/
      % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                     Dload  Upload   Total   Spent    Left  Speed
      0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0*   Trying 127.0.0.1...
    * TCP_NODELAY set
    * Connected to 127.0.0.1 (127.0.0.1) port 8080 (#0)
    > GET / HTTP/1.1
    > Host: 127.0.0.1:8080
    > User-Agent: curl/7.54.0
    > Accept: */*
    > 
    * Empty reply from server
      0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
    * Connection #0 to host 127.0.0.1 left intact
    curl: (52) Empty reply from server


    
### Browsing it 

    # 👀
    http://localhost:8080/
   
### 💀ing it

    docker stop multiplication-app
    docker rm multiplication-app
