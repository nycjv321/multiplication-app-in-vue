import Vue from 'vue'
import Router from 'vue-router'
import MultiplicationApp from '@/components/multiplication-app'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'MultiplicationApp',
      component: MultiplicationApp
    }
  ]
})
