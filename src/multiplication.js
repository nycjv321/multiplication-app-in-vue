const { Decimal } = require('decimal.js')

export const calculate = (numbers) => {
  if (numbers.constructor === Array) {
    return numbers.reduce((acc, number) => {
      if (acc.constructor !== Decimal) {
        return acc
      }
      if (number.constructor === Decimal) {
        return acc.times(number)
      } else {
        return number
      }
    }, new Decimal(1))
  } else {
    return null
  }
}

export class Parser {
  extractInput (text) {
    if (isNaN(text)) {
      let commaSplitter = (lines) => lines.split(',').map(value => value.trim())
      if (text.indexOf('\n') > 0) {
        return text.split('\n').flatMap(line => commaSplitter(line)).filter(input => input !== '')
      } else {
        return commaSplitter(text).filter(input => input !== '')
      }
    } else {
      return [text]
    }
  }

  extractNumbers (text) {
    if ((typeof text) !== 'string') {
      return null
    }
    let numbers = this.extractInput(text)
    let size = numbers.length
    if (size === 0) {
      return []
    } else {
      let parsedNumbers = []
      for (let number of numbers) {
        if (isNaN(number) || (typeof number === 'string' && number.length === 0)) {
          return number
        } else {
          parsedNumbers.push(new Decimal(number))
        }
      }
      return parsedNumbers
    }
  }
}
