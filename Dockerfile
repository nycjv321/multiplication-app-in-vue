FROM node:12.4.0

WORKDIR /usr/src/app
COPY package*.json ./

RUN npm install
COPY . .

RUN npm run build

FROM nginx:1.16.0-alpine
COPY --from=0 /usr/src/app/dist /usr/share/nginx/html/
COPY --from=0 /usr/src/app/node_modules/vue/dist/vue.js /usr/share/nginx/html/vue.js

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
