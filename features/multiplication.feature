Feature: Multiplication App

  Background:
    Given I open the multiplication app's homepage

  Scenario Outline: Multiplication
    When <input> is submitted
    Then the solution should be <total>
    Examples:
      | input                                                 | total                     |
      | -1                                                    | -1                        |
      | -1, -2                                                | 2                         |
      | -1.5, 2.02                                            | -3.03                     |
      | -1.5,{{NEWLINE}}{{NEWLINE}} 2.02{{NEWLINE}}           | -3.03                     |
      | 0                                                     | 0                         |
      | 1                                                     | 1                         |
      |               1       , 2                             | 2                         |
      | 5, 7, 11                                              | 385                       |
      | 5,7,11                                                | 385                       |
      | 1, 2                                                  | 2                         |
      | 1.00005, 2.000000010                                  | 2.0001000100005           |
      | 10, -1, -5, 50                                        | 2500                      |
      | 1000000000000000000000000, 50, 1                      | 5e+25                     |
      | 112345678909876543213456789087654323456789876543, 3.5 | 3.9320987618456790124e+47 |
      | 1{{NEWLINE}},,,5,-100                                 | -500                      |
      | 0{{NEWLINE}}, 1                                       | 0                         |
      | 456787654567898765467876545678765456789876546787654567876545678987654678765456787654567898765467876545678765456789876546787654567876545678987654678765456787654567898765467876545678765456789876546787654567876545678987654678765456787654567898765467876545678765456789876546787654567876545678987654678765456787654567898765467876545678765456789876546787654567876545678987654678765456787654567898765467876545678765456789876546787654567876545678987654678765456787654567898765467876545678765456789876546787654567876545678987654678765456787654567898765467876545678765456789876546787654567876545678987654678765456787654567898765467876545678765456789876546787654567876545678987654678765456787654567898765467876545678765456789876546787654567876545678987654678765{{NEWLINE}}2 | 9.1357530913579753094e+749 |


  Scenario Outline: Multiplication with Bad Input
    When <input> is submitted
    Then an error notification containing a <messsage> should be displayed
    Examples:
      | input                                           | messsage                                                    |
      | a                                               | Provided invalid input: a                                  |
      | 5, 7, 11, Z                                     | Provided invalid input: Z                                  |
      | -1, Ab                                          | Provided invalid input: Ab                                 |
      #  This driver seems to have issues with most emoji(s).
      | 10, -1.0,{{EMOJI::HEART}}, 50                    | Provided invalid input: ♥                                  |
      | 1, {{NEWLINE}}b                                 | Provided invalid input: b                                  |
      | 1000000000z0000, 50, 1                          | Provided invalid input: 1000000000z0000                    |
      | 1, 2 3                                          | Provided invalid input: 2 3                                |
      | 1,~!@$%^&*()_+                                 | Provided invalid input: ~!@$%^&*()_+                      |
      | {{NEWLINE}}~!@$%^&*()_+{{NEWLINE}}             | Provided invalid input: ~!@$%^&*()_+                      |
      | ~!@$%^&*()_+                                   | Provided invalid input: ~!@$%^&*()_+                      |

  Scenario: Multiplication with no Input
    When 1 is submitted
    And the solution is 1
    And the results are cleared
    And {{BLANK}} is submitted
    Then a solution is not displayed
