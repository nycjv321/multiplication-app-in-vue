const { client } = require('nightwatch-cucumber')
const { Given, Then, When } = require('cucumber')

Given(/^I open the multiplication app's homepage$/, () => {
  return client
    .url('http://localhost:' + 8080)
    .waitForElementVisible('body', 1000)
})

Then(/^(.*) is submitted$/, (input) => {
  return client
    .setValue('#numbers', input
      .replace(/{{NEWLINE}}/g, '\n')
      .replace(/{{BLANK}}/g, '')
      .replace('{{EMOJI::HEART}}', '\u2665'))
    .click('#number-btn')
})


Then(/^the solution (?:should be|is) ([^"]*)$/, (total) => {
  return client
    .waitForElementVisible('#result', 1000)
    .assert.containsText('#result', total)
})

Then(/^an error notification containing a ([^"]*) should be displayed$/, (message) => {
  return client
    .waitForElementVisible('#error-notification', 10000)
    .assert
    .containsText('#error-notification', message)
})

Then(/^a solution is not displayed$/, () => {
  return client.waitForElementNotPresent('#error-notification', 1000)
})

Then(/^the results are cleared$/, () => {
  return client
    .clearValue('#numbers')

})
